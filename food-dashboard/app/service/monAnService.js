let BASE_URL = "https://633ec05b0dbc3309f3bc5455.mockapi.io";

export let getListFoodServ = () => {
  return axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  });
};

export let postCreateFoodServ = (food) => {
  return axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data: food,
  });
};

export let deleteFoodByIdServ = (id) => {
  return axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  });
};
