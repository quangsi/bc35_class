export class MonAnV1 {
  constructor(ma, ten, loai, gia, khuyenMai, tinhTrang, hinhAnh, moTa) {
    this.ma = ma;
    this.ten = ten;
    this.loai = loai;
    this.gia = gia;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhAnh = hinhAnh;
    this.moTa = moTa;
  }

  tinhGiaKm = function () {
    return this.gia * (1 - this.khuyenMai);
  };
}
