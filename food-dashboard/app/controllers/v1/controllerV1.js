export let layThongTinTuForm = () => {
  let ma = document.getElementById("foodID").value;
  let ten = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value;
  let giaMon = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang = document.getElementById("tinhTrang").value;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  return { ma, ten, loai, giaMon, khuyenMai, tinhTrang, hinhMon, moTa };
};

export function showThongTinLenForm(data) {
  document.getElementById("imgMonAn").src = data.hinhAnh;

  document.getElementById("spMa").innerText = data.ma;
  document.getElementById("spTenMon").innerText = data.ten;
  document.getElementById("spLoaiMon").innerText = data.loai;
  document.getElementById("spGia").innerText = data.gia;

  document.getElementById("spKM").innerText = data.khuyenMai;
  document.getElementById("spGiaKM").innerText = data.tinhGiaKm();
  document.getElementById("spTT").innerText = data.tinhTrang;
  document.getElementById("pMoTa").innerText = data.moTa;
}
/**
 *  this.ma = ma;
    this.ten = ten;
    this.loai = loai;
    this.gia = gia;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhAnh = hinhAnh;
    this.moTa = moTa;
 */
