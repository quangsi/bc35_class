import { MonAnV1 } from "../../models/v1/modelV1.js";
import { layThongTinTuForm, showThongTinLenForm } from "./controllerV1.js";

function themMon() {
  console.log("yes");
  false;

  let value = layThongTinTuForm();
  let monAn = new MonAnV1(
    value.ma,
    value.ten,
    value.loai,
    value.giaMon,
    value.khuyenMai,
    value.tinhTrang,
    value.hinhMon,
    value.moTa
  );

  showThongTinLenForm(monAn);
}
window.themMon = themMon;

// pass by value ~ pass by reference

//  primitive type ~ reference type

// es6 ~ nhớ tiếng anh

// mutable method and immutable method

// shallwo compare ~ deep compare

// call back ~ event loop ~ async ( bất đồng bộ ) ~ promise all ~ promise chaining
