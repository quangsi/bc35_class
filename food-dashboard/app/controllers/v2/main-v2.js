import { MonAnV2 } from "../../models/v2/modelV2.js";
import {
  deleteFoodByIdServ,
  getListFoodServ,
  postCreateFoodServ,
} from "../../service/monAnService.js";
import { layThongTinTuForm, renderDanhSachMon } from "./controller-v2.js";

// thêm món ăn
document.getElementById("btnThemMon").addEventListener("click", function () {
  let dataForm = layThongTinTuForm();
  console.log("dataForm: ", dataForm);

  postCreateFoodServ(dataForm)
    .then((res) => {
      renderListFoodServ();
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
    });
});

let xoaMonAn = (idFood) => {
  deleteFoodByIdServ(idFood)
    .then((res) => {
      console.log(res);
      renderListFoodServ();
    })
    .catch((err) => {
      console.log(err);
    });
};
window.xoaMonAn = xoaMonAn;

// lần đầu user vào web

let renderListFoodServ = () => {
  getListFoodServ()
    .then((res) => {
      console.log(res);

      // convert dữ liệu từ backend

      let danhSachMon = res.data.map((item) => {
        return new MonAnV2(
          item.id,
          item.name,
          item.type,
          item.price,
          item.discount,
          item.status,
          item.img,
          item.desc
        );
      });

      renderDanhSachMon(danhSachMon);
    })
    .catch((err) => {
      console.log(err);
    });
};
renderListFoodServ();
