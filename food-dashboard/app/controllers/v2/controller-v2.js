export let layThongTinTuForm = () => {
  const ma = document.getElementById("foodID").value;
  const ten = document.getElementById("tenMon").value;
  const loai = document.getElementById("loai").value;
  const moTa = document.getElementById("moTa").value;
  const hinhAnh = document.getElementById("hinhMon").value;
  const tinhTrang = document.getElementById("tinhTrang").value;
  const khuyenMai = document.getElementById("khuyenMai").value;
  const giaMon = document.getElementById("giaMon").value;
  return {
    id: ma,
    name: ten,
    type: loai,
    desc: moTa,
    img: hinhAnh,
    status: tinhTrang,
    discount: khuyenMai,
    price: giaMon,
  };
};

export let renderDanhSachMon = (list) => {
  let contentHTML = "";

  list.forEach((monAn) => {
    contentHTML += `  <tr>
                       <td>${monAn.ma}</td>
                       <td>${monAn.ten}</td>
                       <td>${monAn.loai ? "Chay" : "Mặn"}</td>
                       <td>${monAn.gia}</td>
                       <td>${monAn.khuyenMai} %</td>
                       <td>${
                         monAn.tinhGiaKm() < 0 ? "Miễn phí" : monAn.tinhGiaKm()
                       }</td>
                       <td>${monAn.tinhTrang ? "Còn" : "Hết"}</td>
                       <td>
                       <button 
                       class="btn btn-danger"
                       onclick="xoaMonAn(${monAn.ma})"
                    
                       
                       >Xoá</button></td>
                    </tr> `;
  });
  console.log("contentHTML: ", contentHTML);
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

let username = "Alice";
// let alice = {
//   username: username,
// };

let alice = {
  username,
};
