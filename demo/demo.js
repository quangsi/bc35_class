// function SinhVien(name, age) {
//   this.name = name;
//   this.age = age;
// }

class SinhVien {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  gioiThieu = function () {
    console.log("My name:", this.name);
  };
}

let alice = new SinhVien("alice", 2);
console.log("alice: ", alice);
alice.gioiThieu();
